﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Swordsmith_Player : MonoBehaviour {

    public Game_Swordsmith m_swordsmithManager;

    public Transform m_player;
    public ParticleSystem m_swordParticle;
    public ParticleSystem m_bombParticle;


    public float m_playerSpeed = 2f;
    public float acc = 0.1f;

    public float maxL = -1.87f;
    public float maxR = 1.87f;

    private int movement = 0;
    private float velocity;

    private void Update() {
        if (movement != 0) {
            velocity += acc * movement;
        } else {
            velocity = Mathf.MoveTowards(velocity, 0f, acc * 2);
        }
        velocity = Mathf.Clamp(velocity, -m_playerSpeed, m_playerSpeed);
        m_player.transform.Translate(velocity * Time.deltaTime, 0, 0);

        if (m_player.transform.position.x <= maxL) {
            m_player.transform.position = new Vector3(maxL, m_player.transform.position.y, 0);
        }
        else if (m_player.transform.position.x >= maxR) {
            m_player.transform.position = new Vector3(maxR, m_player.transform.position.y, 0);
        }
    }

    public void moveRight() {
        velocity = 0;
        movement = 1;
    }

    public void moveLeft() {
        velocity = 0;
        movement = -1;
    }

    public void stopMoving() {
        movement = 0;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Interactive")) {
            other.gameObject.SetActive(false);
            m_swordsmithManager.AddPoint();
            m_swordParticle.Play();
        }
        else if (other.CompareTag("Danger")) {
            other.gameObject.SetActive(false);
            m_swordsmithManager.LoseLife();
            m_bombParticle.Play();
        }
    }

}
