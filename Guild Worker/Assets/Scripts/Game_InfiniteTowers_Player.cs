﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class Game_InfiniteTowers_Player : MonoBehaviour {

    public float moveSpeed = 1f;

    private Vector2 m_target = new Vector2();

    public bool moving = false;

    public List<Game_InfiniteTowers_Tower> m_tiles;
    private Game_InfiniteTowers_Tower m_targetTile;

    private void Update() {
        if (moving) {
            transform.position = Vector2.MoveTowards(transform.position, m_target, moveSpeed);
            if ((Vector2)transform.position == m_target) {
                moving = false;
            }
        }
        //Not moving, so build!
        else {
            
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Tile") {
            var t = collision.gameObject.GetComponent<Game_InfiniteTowers_Tower>();
            m_tiles.Add(t);
            ChooseTargetTile();
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "Tile") {
            var t = collision.gameObject.GetComponent<Game_InfiniteTowers_Tower>();
            t.Highlight(false);
            m_tiles.Remove(t);
            ChooseTargetTile();
        }
    }

    public void MoveTowards(Vector2 position) {
        moving = true;
        m_target = position;
    }

    private void ChooseTargetTile() {
        if (m_tiles.Count > 0) {
            float dist = 10000000f;
            for (int i = 0; i < m_tiles.Count; i++) {
                m_tiles[i].Highlight(false);
                float tileDist = Vector2.Distance(this.transform.localPosition, m_tiles[i].transform.position);
                if (tileDist < dist) {
                    dist = tileDist;
                    m_targetTile = m_tiles[i];
                }
            }
        }
        m_targetTile.Highlight(true);
    }

}
