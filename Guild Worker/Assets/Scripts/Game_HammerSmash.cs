﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game_HammerSmash : GameManager {

    public Animator m_thor;
    public TextMeshProUGUI m_timerText;
    public Image m_hitBar;
    public AnimationCurve m_fillCurve;

    public int m_timeLeft = 10;

    public HammerSmashPrize[] m_prizes;

    [Header("Game Over")]
    public GameObject m_gameOver;

    private int m_taps = 0;

    private float m_timerTick = 0f;

    private int m_wonPrizeTier = -1;


    private void Start() {
        StartGame(GuildJobs.GuildLeader.MightyLions);
        m_timerText.text = m_timeLeft.ToString();
    }

    private void Update() {
        if (m_gamePlaying) {
            m_timerTick += Time.deltaTime;
            if (m_timerTick >= 1f) {
                m_timeLeft--;
                m_timerText.text = m_timeLeft.ToString();
                m_timerTick = 0f;
                if (m_timeLeft <= 0) {
                    EndGame();
                }
            }
        }
    }

    public override void EndGame() {
        base.EndGame();
        m_wonPrizeTier = Smash();
        m_payoutWorth = m_prizes[m_wonPrizeTier].m_prize;
    }

    public void smashCallback() {
        StartCoroutine(fillRoutine());

        Debug.Log("Taps: " + m_taps);
    }

    private int Smash() {
        m_thor.SetTrigger("Smash");
        for (int i = 0; i < m_prizes.Length; i++) {
            if (m_taps < m_prizes[i].m_taps) {
                return i;
            }
        }
        return (m_prizes.Length - 1);
    }

    public void addTap() {
        m_taps++;
    }

    private IEnumerator fillRoutine () {

        float endFill = (float)m_wonPrizeTier / (float)m_prizes.Length;
        float step = 0f;
        while (step < 1f) {
            step += Time.deltaTime;
            m_hitBar.fillAmount = m_fillCurve.Evaluate(step) * endFill;
            yield return null;
        }

        m_hitBar.fillAmount = endFill;

        yield return new WaitForSeconds(2f);
        m_gameOver.SetActive(true);
        yield return null;
    }

    [System.Serializable]
    public class HammerSmashPrize {
        public int m_taps = 50;
        public int m_prize = 5;
    }


}
