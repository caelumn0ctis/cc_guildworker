﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_InfiniteTowers_Tower : MonoBehaviour {

    public SpriteRenderer m_highlight;
    public SpriteRenderer m_tower;

    public int m_experience = 0;

    public bool m_highlighted = false;
    public bool m_turret = false;

    // Use this for initialization
    void Start () {
        CheckHighlight();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            m_highlighted = true;
            CheckHighlight();
        }
    }

    private void OnTriggerExit2D (Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            m_highlighted = false;
            CheckHighlight();
        }
    }

    public void Highlight(bool set) {
        m_highlighted = set;
        CheckHighlight();
    }

    public void GiveXP(int give) {
        m_experience += give;
        UpdateXP();
    }

    private void UpdateXP() {
        //If enough xp. Have tower.
        if (m_experience >= 300) {
            m_tower.enabled = true;
            m_turret = true;
        }
    }

    private void CheckHighlight() {
        if (m_highlighted) {
            m_highlight.enabled = true;
        } else {
            m_highlight.enabled = false;
        }
    }
}
