﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerGoldDisplay : MonoBehaviour {

    public TextMeshProUGUI m_text;

    private void Start() {
        m_text.text = PlayerManager.instance.m_gold.ToString();
    }

}
