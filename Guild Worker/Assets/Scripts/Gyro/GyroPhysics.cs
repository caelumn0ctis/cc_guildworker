﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroPhysics : MonoBehaviour {

    public GameObject m_gyroBallPrefab;
    public int m_ballCount;

    List<GameObject> m_balls = new List<GameObject>();

    Gyroscope m_gyro;
    public Vector3 m_gravity;

    WebCamTexture m_webCamTexture;

    private void Start() {
        WebCamTexture webcamTexture = new WebCamTexture();
        for (int i = 0; i < m_ballCount; i++) {
            GameObject b = Instantiate(m_gyroBallPrefab, transform);
            b.transform.position = new Vector3(Random.Range(-5f, 5f), Random.Range(-4f, 4f), b.transform.position.z);
            b.transform.Rotate(0, Random.Range(0, 360), 0);
            b.GetComponent<Renderer>().material.mainTexture = webcamTexture;
        }
        m_gyro = Input.gyro;
        Input.gyro.enabled = true;
        webcamTexture.Play();
    }

    void Update () {
#if UNITY_EDITOR
        Physics2D.gravity = m_gravity;
#else
        Physics2D.gravity = m_gyro.gravity * 9f;
#endif
    }
}
