﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class WorkButton : MonoBehaviour {

    public TextMeshProUGUI m_workName;
    public Image m_guildleader;

    public Sprite[] m_guildLeaderSprites;

    public void setOffer(JobOffer offer) {
        m_guildleader.sprite = m_guildLeaderSprites[(int)offer.m_guildLeader];
        m_workName.text = GuildJobs.instance.getJob(offer.m_currentJob).m_jobName;
    }

}
