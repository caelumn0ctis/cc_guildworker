﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Game_Coincatch : GameManager {

    [Header("VARIABLES")]
    public float m_TIME = 60f;
    public AnimationCurve m_spawnCurve;

    [Header("REFERENCES")]
    public List<Game_Coincatch_Coin> m_coins = new List<Game_Coincatch_Coin>();

    public TextMeshProUGUI m_pointText;
    public TextMeshProUGUI m_timerText;

    public GameObject m_gameOver;


    private float m_timer = 0;
    private int m_points = 0;

    private float m_coinCooldown = 0f;

    private void Start() {
        m_timer = m_TIME;
        StartGame(GuildJobs.GuildLeader.Whiskers);
    }

    private void Update() {
        if (m_gamePlaying) {
            m_timer -= Time.deltaTime;
            int t = (int)m_timer;
            m_timerText.text = t.ToString();

            if (m_timer <= 0f) {
                gameOver();
            }

            m_coinCooldown -= Time.deltaTime;
            if (m_coinCooldown <= 0) {
                spawnCoin();
            }
        }
    }

    private void spawnCoin() {
        m_coinCooldown = getCoinCooldown();

        List<int> availableCoins = new List<int>();
        for (int i = 0; i < m_coins.Count; i++) {
            if (!m_coins[i].m_alive) {
                availableCoins.Add(i);
            }
        }
        //Got coins, random one.
        int rand = Random.Range(0, availableCoins.Count);
        m_coins[rand].Fly();
    }

    public void CatchCoin() {
        m_points++;
        updatePoints();
    }

    private float getCoinCooldown() {
        float diff = m_timer / m_TIME;
        float f = m_spawnCurve.Evaluate(diff);
        return f;
    }

    private void gameOver() {
        m_gamePlaying = false;
        m_gameOver.SetActive(true);
    }

    private void updatePoints() {
        m_pointText.text = m_points.ToString();
    }

    public override void GoToPayout() {
        calculateCash(m_points);
        base.GoToPayout();
    }

    public override void calculateCash(float points) {
        float f = points / 10f;
        int pay = (int)f;
        m_payoutWorth = pay;
    }
}
