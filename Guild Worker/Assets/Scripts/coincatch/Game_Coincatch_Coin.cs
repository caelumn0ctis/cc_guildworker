﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Game_Coincatch_Coin : MonoBehaviour {

    public Animator m_animator;
    public Image m_img;
    public ParticleSystem m_grabParticle;
    [HideInInspector]
    public bool m_alive = false;

    public void Fly() {
        m_img.enabled = true;
        m_animator.SetTrigger("Fly");
        m_alive = true;
    }

    public void Grabbed() {
        m_animator.SetTrigger("Grabbed");
        m_alive = false;
        m_grabParticle.Play();
        m_img.enabled = false;
    }

}
