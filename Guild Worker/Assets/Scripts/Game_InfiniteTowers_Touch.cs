﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_InfiniteTowers_Touch : MonoBehaviour {

    public Game_InfiniteTowers_Player m_player;

	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0) {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began) {
                m_player.MoveTowards(Camera.main.ScreenToWorldPoint(t.position));
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            m_player.MoveTowards(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}
