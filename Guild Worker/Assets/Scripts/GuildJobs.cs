﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class GuildJobs : ScriptableObject {

    public enum GuildLeader {MightyLions, Whiskers, Zenpaws, SIZE};

    [SerializeField]
    private List<GuildContractor> guildContractors = new List<GuildContractor>();

    public List<GuildJob> getJobsFromContractor(GuildJobs.GuildLeader contractor) {
        for (int i = 0; i < guildContractors.Count; i++) {
            if (guildContractors[i].m_guildLeader == contractor) {
                return guildContractors[i].m_jobs;
            }
        }
        return null;
    }

    public GuildJob getJob(string job) {
        for (int i = 0; i < guildContractors.Count; i++) {
            for (int j= 0; j < guildContractors[i].m_jobs.Count; j++) {
                if (guildContractors[i].m_jobs[j].m_jobID.Contains(job)) {
                    return guildContractors[i].m_jobs[j];
                }
            }
        }
        Debug.Log("Job not found in database");
        return null;
    }

    public GuildContractor getContractor(GuildLeader con) {
        for (int i = 0; i < guildContractors.Count; i++) {
            if (guildContractors[i].m_guildLeader == con) {
                return guildContractors[i];
            }
        }
        return guildContractors[0];
    }

    //
    private static GuildJobs _instance;
    public static GuildJobs instance {
        get {
            if (_instance == null) {
                _instance = Resources.Load<GuildJobs>("GuildJobs");
            }
            return _instance;
        }
    }

}

[System.Serializable]
public class GuildContractor {
    public GuildJobs.GuildLeader m_guildLeader;
    public Sprite m_leaderSprite;
    public List<GuildJob> m_jobs;
}

[System.Serializable]
public class GuildJob {
    public string m_jobID;
    public string m_jobName;
    public Sprite m_jobImage;
}
