﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public float speed = 0.1f;
    private int dir = 1;

	// Update is called once per frame
	void Update () {

        if (transform.position.y > 2.342) {
            dir = -1;
        }
        if (transform.position.y < -2.055) {
            dir = 1;
        }
        
        transform.position = new Vector2(transform.position.x, transform.position.y + (dir * speed));
	}
}
