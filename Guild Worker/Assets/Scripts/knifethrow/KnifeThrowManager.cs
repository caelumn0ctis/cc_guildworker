﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnifeThrowManager : MonoBehaviour {

    public Transform aim;
    public Transform startObj;
    public Rigidbody2D knife;
    public float distMax = 10;
    public Text m_scoreText;
    public Text m_timeText;
    public float rotation = 15;
    [Range(0f, 1f)]
    public float force = 1;

    public int m_score = 0;
    public GameObject restartButton;

    private int m_time = 30;
    private Camera mainCamera;
    private Vector2 startpos;
    private Quaternion startquaternion;
    private bool flying = false;
    private Vector2 mousePos;
    private Vector2 dirVec;

    private bool hasEnded = false;

    // Use this for initialization
    void Start() {
        mainCamera = Camera.main;
        startpos = knife.transform.position;
        startquaternion = knife.transform.rotation;
        StartCoroutine(coTimer());
    }

    // Update is called once per frame
    void FixedUpdate() {

        //throw
        if (!flying) {
            if (Input.GetMouseButton(0)) {
                if (!aim.gameObject.activeInHierarchy) {
                    aim.gameObject.SetActive(true);
                }
                //Input.mousePosition;
                mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                float dist = Vector2.Distance(mousePos, startObj.position);
                dirVec = mousePos;

                if (dist < distMax) {
                    aim.position = dirVec;
                    //aim.position = new Vector3(aim.position.x, aim.position.y, 0);
                } else {
                    dirVec = (Input.mousePosition).normalized * distMax;// * dist;
                    aim.position = (dirVec + (Vector2)startObj.position);
                }
            }
            if (!hasEnded) {
                if (Input.GetMouseButtonUp(0)) {
                    aim.gameObject.SetActive(false);
                    flying = true;
                    knife.gravityScale = 1;
                    Vector2 path = aim.position - startObj.position;
                    //Debug.Log("path " + path);
                    knife.AddForce(path * (200 * force));
                    //knife.transform.Rotate(path);
                    //knife.AddTorque(45 * 10000);
                    knife.AddTorque(-(Vector3.Angle(startObj.position, aim.position) / rotation));
                }
            }
        }

        //reset
        if (knife.transform.position.y < -6) {
            resetKnife();
        }

        if (knife.transform.position.x < -5 || knife.transform.position.x > 5) {
            resetKnife();
        }
    }

    IEnumerator coTimer() {
        while (m_time > 0) {
            CountTime();
            yield return new WaitForSeconds(1);
        }
        EndGame();
    }

    //end the game
    public void EndGame() {
        Debug.Log("GAME HAS ENDED");
        hasEnded = true;
        restartButton.SetActive(true);
    }

    public void resetKnife() {
        knife.velocity = new Vector3(0f, 0f, 0f);
        knife.angularVelocity = 0;
        knife.transform.position = startpos;
        knife.transform.rotation = startquaternion;
        flying = false;
    }

    public void restart() {
        restartButton.SetActive(false);
        hasEnded = false;
        flying = false;
        m_score = 0;
        m_time = 30;
        m_scoreText.text = "Score: " + m_score;
        m_timeText.text = "Time: " + m_time;
        StartCoroutine(coTimer());
    }

    public void score() {
        m_score++;
        m_scoreText.text = "Score: " + m_score;
    }

    public void CountTime() {
        m_time--;
        m_timeText.text = "Time: " + m_time;
    }
}
