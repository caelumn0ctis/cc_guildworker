﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaft : MonoBehaviour {

    public KnifeThrowManager m_knifeThrowManager;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Start") {
            return;
        }
        m_knifeThrowManager.resetKnife();
    }
}
