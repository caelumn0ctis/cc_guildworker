﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour {

    public Transform parent;
    public KnifeThrowManager m_knifeThrowManager;

    //private void OnCollisionEnter2D(Collision2D collision) {
    //    Debug.Log("OnCollisionEnter");
    //    if (collision.gameObject.tag == "Target") {
    //        Debug.Log("OnCollisionEnter ASDIHJBASFKJBAKSF");
    //    }
    //}
    
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Target") {
            parent.GetComponent<Rigidbody2D>().simulated = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            parent.SetParent(other.transform);
            parent.localPosition = new Vector2(-0.132f, 0);
            parent.rotation = Quaternion.Euler(0,0,90);
            StartCoroutine(coWaitAndReset());
            m_knifeThrowManager.score();
            return;
        }else if (other.gameObject.tag == "Start") {
            return;
        }else if (other.gameObject.tag == "Tree") {
            m_knifeThrowManager.resetKnife();
        }
        //m_knifeThrowManager.resetKnife();
    }

    IEnumerator coWaitAndReset() {
        yield return new WaitForSeconds(1f);
        parent.GetComponent<Rigidbody2D>().simulated = true;
        parent.SetParent(null);
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        m_knifeThrowManager.resetKnife();
    }
}
