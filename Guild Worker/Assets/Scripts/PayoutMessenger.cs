﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayoutMessenger {

    private static PayoutMessenger _instance;
    public static PayoutMessenger instance {
        get {
            if (_instance == null) {
                _instance = new PayoutMessenger();
            }
            return _instance;
        }
    }

    private int m_payoutCash = 0;
    private GuildJobs.GuildLeader m_contractor = GuildJobs.GuildLeader.MightyLions;

    public void setPayoutCash(float check) {
        m_payoutCash = (int)check;
    }

    public int getPayoutCash() {
        return m_payoutCash;
    }

    public void setContractor(GuildJobs.GuildLeader gl) {
        m_contractor = gl;
    }

    public GuildJobs.GuildLeader getContractor() {
        return m_contractor;
    }

}
