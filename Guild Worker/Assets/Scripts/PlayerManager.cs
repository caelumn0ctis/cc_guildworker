﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager {

    private static PlayerManager _instance;
    public static PlayerManager instance {
        get {
            if (_instance == null) {
                _instance = new PlayerManager();
            }
            return _instance;
        }
    }

    public int m_gold = 0;
    public List<string> m_unlockedJobs = new List<string>();

    PlayerManager() {
        m_unlockedJobs.Add("swordsmith");
        m_unlockedJobs.Add("coincatch");
        m_unlockedJobs.Add("scrapswat");
        Load();
    }

    public void Save() {
        //Save Gold.
        PlayerPrefs.SetInt("Gold", m_gold);
        
        //Save Unlocked Jobs
        for (int i = 0; i < m_unlockedJobs.Count; i++) {
            string s = m_unlockedJobs[i];
            PlayerPrefs.SetInt("Job_" + s, 1);
        }

        PlayerPrefs.Save();
    }

    public void Load() {
        m_gold = PlayerPrefs.GetInt("Gold", 0);
    }

}
