﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;


public class GachaMenu : MonoBehaviour {

    public enum GachaReward {NONE, CollectibleCommon, CollectibleEpic, CollectibleMythic, GuildJobCommon, GuildJobEpic, GuildJobMythic, SIZE};

    public GameObject m_parent;

    public Image m_boxImage;
    public TextMeshProUGUI m_price;
    public Button m_buyButton;

    private List<GachaBox> m_boxes = new List<GachaBox>();
    private int m_boxIndex;

    private bool m_loaded = false;

    [Header("Purchase Complete")]
    public GameObject m_purchaseBox;
    public TextMeshProUGUI m_purchaseText;

	public void Show() {
        m_parent.SetActive(true);
        LoadBoxes();
        ShowBox();
    }

    public void Hide() {
        m_parent.SetActive(false);
    }

    public void NewBox(bool increase) {
        if (increase) {
            m_boxIndex++;
            if (m_boxIndex >= m_boxes.Count) {
                m_boxIndex = 0;
            }
        }
        else {
            m_boxIndex--;
            if (m_boxIndex < 0) {
                m_boxIndex = m_boxes.Count-1;
            }
        }
        ShowBox();
    }

    public void BuyBox() {
        PlayerManager.instance.m_gold -= m_boxes[m_boxIndex].m_goldCost;
        m_purchaseBox.SetActive(true);
        var rew = m_boxes[m_boxIndex].RandomReward();
        m_purchaseText.text = "You get: " + rew;
    }

    private void ShowBox() {
        GachaBox box = m_boxes[m_boxIndex];
        m_boxImage.sprite = box.m_boxSprite;
        m_price.text = box.m_goldCost.ToString();
        CheckPrice();
        Debug.Log("Showing Box: " + box.m_boxID);
    }

    private void CheckPrice() {
        m_buyButton.interactable = PlayerManager.instance.m_gold >= m_boxes[m_boxIndex].m_goldCost;
    }

    private void LoadBoxes() {
        if (!m_loaded) {
            var gachas = Resources.LoadAll("GachaBoxes", typeof(GachaBox));
            foreach (GachaBox gb in gachas) {
                m_boxes.Add(gb);
            }
            m_boxIndex = 0;
        }
    }
}
