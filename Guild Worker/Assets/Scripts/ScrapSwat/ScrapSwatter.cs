﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapSwatter : MonoBehaviour {

    [Header("Values")]
    public float m_minSwatVelocity = .001f;
    public float swatStrength = 5f;
    [Header("References")]
    public GameObject m_swatTrailPrefab;
    public Rigidbody2D m_rb;
    public Camera m_cam;
    public CircleCollider2D m_circleCollider;

    [HideInInspector()]
    public Vector2 m_currentDirection;

    private bool m_isSwatting = false;
    private Vector2 m_previousPosition;

    private GameObject m_currentSwatTrail;

    private void Update() {
        if(Input.GetMouseButtonDown(0)) {
            StarSwatting();
        } else if(Input.GetMouseButtonUp(0)) {
            StopSwatting();
        }

        if(m_isSwatting) {
            UpdateCut();
        }
    }

    private void UpdateCut() {
        Vector2 newPos = m_cam.ScreenToWorldPoint(Input.mousePosition);
        m_rb.position = newPos;
        float velocity = (newPos - m_previousPosition).magnitude * Time.deltaTime;
        //Debug.Log("Velociy: " + velocity.ToString());
        if(velocity > m_minSwatVelocity) {
            m_circleCollider.enabled = true;
        } else {
            m_circleCollider.enabled = false;
        }

        Vector2 dir = m_previousPosition - newPos;
        m_currentDirection = dir.normalized;

        m_previousPosition = newPos;
    }


    private void StarSwatting() {
        m_isSwatting = true;
        m_previousPosition = m_cam.ScreenToWorldPoint(Input.mousePosition);
        m_currentSwatTrail = Instantiate(m_swatTrailPrefab, transform);
        m_circleCollider.enabled = false;
    }

    private void StopSwatting() {
        m_isSwatting = false;
        m_currentSwatTrail.transform.SetParent(null);
        Destroy(m_currentSwatTrail, 2f);
        m_circleCollider.enabled = false;
    }
}
