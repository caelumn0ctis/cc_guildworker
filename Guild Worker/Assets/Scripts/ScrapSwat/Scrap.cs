﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrap : MonoBehaviour {
    public float m_startForce = 15f;

    public Rigidbody2D m_rb;

    public void Start() {
        m_rb.AddForce(-transform.up * m_startForce, ForceMode2D.Impulse);
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Swat") {
            Debug.Log("Hit");
            Vector3 direction = (col.transform.position - transform.position).normalized;

            m_rb.velocity *= 0;

            var s = col.GetComponent<ScrapSwatter>();
            m_rb.AddForce(-s.m_currentDirection * s.swatStrength, ForceMode2D.Impulse);
            

            Destroy(gameObject, 1f);
        }
    }

}
