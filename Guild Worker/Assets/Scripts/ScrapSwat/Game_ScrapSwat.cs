﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_ScrapSwat : GameManager {

    public GameObject m_scrapPrefab;
    public Transform[] m_spawnPoints;

    public float m_minDelay = 0.1f;
    public float m_maxDelay = 1f;

    private void Start() {
        StartGame(GuildJobs.GuildLeader.Zenpaws);
        StartCoroutine(SpawnScrap());
    }

    private IEnumerator SpawnScrap() {
        while (true) {
            float delay = Random.Range(m_minDelay, m_maxDelay);
            yield return new WaitForSeconds(delay);

            int spawnIndex = Random.Range(0, m_spawnPoints.Length);
            Transform spawnPoint = m_spawnPoints[spawnIndex];

            GameObject scrap = Instantiate(m_scrapPrefab, spawnPoint.position, spawnPoint.rotation);
            Destroy(scrap, 5f);
        }
    }

}
