﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game_Swordsmith : GameManager {

    public Transform m_blacksmith;
    public Rigidbody2D[] m_swords;
    public Rigidbody2D[] m_bombs;

    public TextMeshProUGUI m_pointText;

    public Image[] m_hearts;

    public GameObject m_gameOver;

    private int m_swordIndex = 0;
    private int m_bombIndex = 0;

    private float m_throwCooldown = 0;

    private int m_points = 0;
    private int m_lives = 3;

    private void Start() {
        StartGame(GuildJobs.GuildLeader.MightyLions);
    }

    public void AddPoint() {
        m_points++;
        m_pointText.text = m_points.ToString();
    }

    public void LoseLife() {
        m_lives--;
        m_hearts[m_lives].enabled = false;
        if (m_lives <= 0) {
            gameOver();
        }
    }

    private void Update() {
        if (m_gamePlaying) {
            m_throwCooldown -= Time.deltaTime;
            if (m_throwCooldown <= 0) {
                if (swordThrowTest()) {
                    ThrowSword();
                }
                else {
                    ThrowBomb();
                }
            }
        }
    }

    private void ThrowSword() {
        m_throwCooldown += getCooldown();
        Rigidbody2D t = m_swords[m_swordIndex];
        t.velocity = Vector2.zero;
        t.transform.position = m_blacksmith.position;
        t.gameObject.SetActive(true);
        Vector2 angle = new Vector2(Random.Range(-0.1f, 2.5f), Random.Range(6f, 9f));
        t.AddForce(angle, ForceMode2D.Impulse);

        m_swordIndex++;
        m_swordIndex = m_swordIndex % m_swords.Length;
    }

    private void ThrowBomb() {
        m_throwCooldown += getCooldown();
        Rigidbody2D t = m_bombs[m_bombIndex];
        t.velocity = Vector2.zero;
        t.transform.position = m_blacksmith.position;
        t.gameObject.SetActive(true);
        Vector2 angle = new Vector2(Random.Range(-0.1f, 2.5f), Random.Range(6f, 9f));
        t.AddForce(angle, ForceMode2D.Impulse);

        m_bombIndex++;
        m_bombIndex = m_bombIndex % m_bombs.Length;
    }

    private float getCooldown() {
        float f = 0f;

        float minMin = 0.5f;
        float minMax = 2.5f;

        float minDiff = minMax - minMin;

        float maxMin = 0.8f;
        float maxMax = 3.0f;

        float maxDiff = maxMax - maxMin;

        float ratio = (float)m_points / 30;

        ratio = Mathf.Clamp(ratio, 0f, 1f);


        f = Random.Range(minMax - (ratio*minDiff) , maxMax - (ratio*maxDiff));

        Debug.Log("COOLDOWN: " + f);
        return f;
    }

    private bool swordThrowTest() {
        int rand = Random.Range(0, 100);
        int bombChance = 5 + m_points;

        bombChance = Mathf.Clamp(bombChance, 5, 40);
        if (rand < bombChance) {
            return false;
        }

        return true;
    }

    private void gameOver () {
        for (int i = 0; i < m_swords.Length; i++) {
            m_swords[i].gameObject.SetActive(false);
        }
        m_gamePlaying = false;
        m_gameOver.SetActive(true);
    }

    public override void GoToPayout() {
        calculateCash(m_points);
        base.GoToPayout();
    }

    public override void calculateCash(float points) {
        float f = points / 10f;
        int pay = (int)f;
        m_payoutWorth = pay;
    }

}
