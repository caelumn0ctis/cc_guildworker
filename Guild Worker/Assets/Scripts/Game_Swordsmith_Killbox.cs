﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Swordsmith_Killbox : MonoBehaviour {

    public Game_Swordsmith m_swordsmithManager;


    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Interactive")) {
            other.gameObject.SetActive(false);
            m_swordsmithManager.LoseLife();
        }
        else if (other.CompareTag("Danger")) {
            other.gameObject.SetActive(false);
        }
    }
}
