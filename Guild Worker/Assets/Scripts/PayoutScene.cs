﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PayoutScene : MonoBehaviour {

    public TextMeshProUGUI m_amount;
    public Image m_image;

    private int m_cash;


    private void Start() {
        m_cash = PayoutMessenger.instance.getPayoutCash();
        PayoutMessenger.instance.setPayoutCash(0);
        m_amount.text = m_cash.ToString();

        m_image.sprite = GuildJobs.instance.getContractor(PayoutMessenger.instance.getContractor()).m_leaderSprite;
    }

    public void collectPayment() {
        PlayerManager.instance.m_gold += m_cash;
        SceneManager.LoadScene("Main");
    }
}
