﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectionMenu_JobDisplay : MonoBehaviour {

    public Color[] m_cardColors;
    public Color m_lockedColor;
    public Sprite m_lockedImage;

    public TextMeshProUGUI m_name;
    public Image m_card;
    public Image m_jobImage;

    public void Setup(GuildJobs.GuildLeader guild, GuildJob job) {
        gameObject.SetActive(true);

        //Unlocked.
        if (PlayerManager.instance.m_unlockedJobs.Contains(job.m_jobID)) {
            m_card.color = m_cardColors[(int)guild];
            m_jobImage.sprite = job.m_jobImage;
            m_name.text = job.m_jobName;
        }
        //Locked.
        else {
            m_card.color = m_lockedColor;
            m_jobImage.sprite = m_lockedImage;
            m_name.text = "???";
        }

    }

}
