﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectionMenu : MonoBehaviour {

    public GameObject m_parent;

    public List<CollectionMenu_JobDisplay> m_jobDisplays;

    public void Show() {
        m_parent.SetActive(true);

        for (int i = 0; i < m_jobDisplays.Count; i++) {
            m_jobDisplays[i].gameObject.SetActive(false);
        }

        int dispIndex = 0;
        for (int i = 0; i < (int)GuildJobs.GuildLeader.SIZE; i++) {
            GuildJobs.GuildLeader guild = (GuildJobs.GuildLeader)i;
            List<GuildJob> jobs = GuildJobs.instance.getJobsFromContractor(guild);
            for (int j = 0; j < jobs.Count; j++) {
                m_jobDisplays[dispIndex].Setup(guild, jobs[j]);
                dispIndex++;
            }
        }

    }

    public void Hide() {
        m_parent.SetActive(false);
    }

}
