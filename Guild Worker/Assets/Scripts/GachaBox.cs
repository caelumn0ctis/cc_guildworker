﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GachaBox: ScriptableObject {

    public string m_boxID;
    public int m_goldCost;
    public Sprite m_boxSprite;
    public List<GachaBoxRewardInfo> m_rewards;

    public GachaMenu.GachaReward RandomReward() {
        int count = 0;
        for (int i = 0; i < m_rewards.Count; i++) {
            count += m_rewards[i].m_tickets;
        }
        int roll = Random.Range(0, count);
        int check = 0;
        for (int i= 0; i < m_rewards.Count; i++) {
            if (check <= roll) {
                return m_rewards[i].m_rewardType;
            }
            check += m_rewards[i].m_tickets;
        }

        //This is error.
        return m_rewards[0].m_rewardType;
    }

}

[System.Serializable]
public class GachaBoxRewardInfo {
    public GachaMenu.GachaReward m_rewardType;
    public int m_tickets;
}