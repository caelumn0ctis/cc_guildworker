﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool m_gamePlaying = false;
    
    [HideInInspector]
    public int m_payoutWorth = 0;

	public virtual void StartGame(GuildJobs.GuildLeader contractor) {
        PayoutMessenger.instance.setContractor(contractor);
        m_gamePlaying = true;
    }

    public virtual void EndGame() {
        m_gamePlaying = false;
    }

    public virtual void calculateCash(float points) {
        m_payoutWorth = 1;
    }

    public virtual void GoToPayout() {
        PayoutMessenger.instance.setPayoutCash(m_payoutWorth);
        //GoTo Payout scene.
        SceneManager.LoadScene("PayOut");
    }
}
