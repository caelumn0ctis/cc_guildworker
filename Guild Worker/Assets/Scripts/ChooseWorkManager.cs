﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseWorkManager : MonoBehaviour {


    public List<JobOffer> m_offers;

    public GameObject m_parent;
    public WorkButton m_workButton;

    private JobOffer m_currentOffer;
    private int m_curr = 0;

	public void Show() {
        m_parent.SetActive(true);
        getCurrentJobs();
        m_currentOffer = m_offers[m_curr];
        //Set job.
        m_workButton.setOffer(m_currentOffer);
    }

    public void Hide() {
        m_parent.SetActive(false);
    }

    public void takeOffer() {
        SceneManager.LoadScene("Job_" + m_currentOffer.m_currentJob);
    }

    public void changeOffer(bool back) {
        if (back) {
            m_curr -= 1;
            if (m_curr < 0) {
                m_curr = m_offers.Count - 1;
            }
        }
        else {
            m_curr += 1;
            if (m_curr >= m_offers.Count) {
                m_curr = 0;
            }
        }
        m_currentOffer = m_offers[m_curr];
        m_workButton.setOffer(m_currentOffer);
    }

    private void getCurrentJobs() {
        for (int i =0; i < m_offers.Count; i++) {
            var d = GuildJobs.instance.getJobsFromContractor(m_offers[i].m_guildLeader);
            for (int j = 0; j < d.Count; j++) {
                if (PlayerManager.instance.m_unlockedJobs.Contains(d[j].m_jobID)) {
                    m_offers[i].m_availableJobs.Add(d[j].m_jobID);
                }
            }
            //Job offers gathered. Make one current.
            int rand = Random.Range(0, m_offers[i].m_availableJobs.Count);
            m_offers[i].setCurrentJob(m_offers[i].m_availableJobs[rand]);
        }
    }

}

[System.Serializable]
public class JobOffer {
    public GuildJobs.GuildLeader m_guildLeader;
    public string m_currentJob;
    public List<string> m_availableJobs;

    public void setCurrentJob(string jobID) {
        m_currentJob = jobID;
    }
}
